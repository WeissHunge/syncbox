CC=gcc
CFLAGS=-lpthread -Wextra -Wall -I./headers
obj=./bin
src=./src

all: util.o sync_client.o  server.o client.o
	gcc -o server $(obj)/server.o $(obj)/util.o $(CFLAGS)
	gcc -o client $(obj)/client.o $(obj)/util.o $(obj)/sync_client.o $(CFLAGS)

util.o: $(src)/util.c
	gcc -c -o $(obj)/$@ $< $(CFLAGS)

server.o: $(src)/server.c
	gcc -c -o $(obj)/$@ $< $(CFLAGS)

client.o: $(src)/client.c
	gcc -c -o $(obj)/$@ $< $(CFLAGS)

sync_client.o: $(src)/sync_client.c
	gcc -c -o $(obj)/$@ $< $(CFLAGS)
	
clean:
	rm -rf $(obj)/*
	rm -rf server client
