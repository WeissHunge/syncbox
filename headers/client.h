#ifndef CLIENT_H
#define CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include "util.h"
#include "sync_client.h"

#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define EVENT_BUF_LEN     ( sizeof (struct inotify_event) )*50

int get_file_from_server(char *name,int sockfd);
int menu(int sockfd);
int get_input(char *nome_arq);
int get_sync_dir();
int send_file_to_server(char *name,int sockfd);
int get_server_file_list(struct file_list *fl,int sockfd);
int remove_file_from_server(char *name,int sockfd);

#endif