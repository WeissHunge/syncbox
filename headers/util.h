#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>

#define BUFFER_SIZE 1250
#define MAX_NAME 256
#define MAX_FILES 256
#define DEVICES 2

#define NOT_FOUND "nao encontrado"
#define FOUND "encontrado"

#define DELETED "deletado"
#define NOT_DELETED "nao deletado"

#define ERR_SOCKET_W "erro na escrita do socket"
#define ERR_SOCKET_R "erro na leitura do socket"

#define CONNECTED "conectado"
#define DISCONNECTED "desconectado"

#define UPLOAD_REQUEST 1
#define DOWNLOAD_REQUEST 2
#define LIST_SERVER_REQUEST 3
#define LIST_CLIENT_REQUEST 4
#define SYNC_REQUEST 5
#define EXIT_REQUEST 6
#define GET_SYNC_DIR 7
#define REMOVE_REQUEST 8

struct	file_info	{
						char path[MAX_NAME];
						char name[MAX_NAME];
						long int last_modified;
						long int size;
};

struct file_list
{
	struct	file_info f[MAX_FILES];
	int length;
};

struct	client	{
						char userid[MAX_NAME];
						int devices[DEVICES];
						int lst_dev;
						sem_t acess_client;
						int leitores[MAX_FILES];
						int logged_in;
						
						struct file_list arquivos;
						sem_t acess_file[MAX_FILES];
};


typedef struct item{

	struct client cliente;
	struct item * proximo;
}fila;

int create_folder(char *name);
int list_files(char *dir_alvo,struct file_list *);
void print_file_list(struct file_list);
int send_file(char *name,int socketid);
int receive_file(char *name,int socketid);


void addCliente(char * id,char *dir);
void removeCliente(char * id);
int existClient(char * id);
int existClientInQ(fila * atual,char * id);
fila * getClientInQ(fila * atual,char * id);
fila * getClient(char * id);

int writeSemWait(char * arq,fila * cliente);
int readSemWait(char * arq,fila * cliente);
int writeSemPost(int i,fila * cliente);
int readSemPost(int i,fila * cliente);

#endif