#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>

#include "util.h"

#define MAX_CONNECTIONS 12

int send_file_to_client(char *path,int sockfd,fila *atual);
int delete_file_from_client(char *path,int sockfd,fila *atual);
int get_file_from_client(char *path,int sockfd,fila *atual);
int send_file_list(char *path,int sockfd,fila *atual);

#endif