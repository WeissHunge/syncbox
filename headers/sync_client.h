#ifndef SYNC_CLIENT_H
#define SYNC_CLIENT_H 

#ifndef CLIENT_H
#include "client.h"
#endif

#include "util.h"

#include <sys/inotify.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <semaphore.h>
#include <arpa/inet.h>


void* keepServerSyncSoldier(void *p1);
int syncDiffFilesList(struct file_list* client_fl,struct file_list* server_fl,int sockfd);
void* keepClientSyncSoldier(void *p1);
int doIhaveToSync(int);
int remove_notify();
int add_notify();

#endif