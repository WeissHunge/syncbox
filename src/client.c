#include "client.h"

char user_folder[MAX_NAME];
char user_name[MAX_NAME];

sem_t operacao_completa;

int get_input(char *nome_arq){


	char buffer[BUFFER_SIZE],comando[BUFFER_SIZE];
	fgets(buffer,BUFFER_SIZE,stdin);

	sscanf(buffer,"%s %s",comando,nome_arq);
	if(strcmp(comando,"upload")==0)
		return UPLOAD_REQUEST;
	if(strcmp(comando,"download")==0)
		return DOWNLOAD_REQUEST;
	if(strcmp(comando,"list_server")==0)
		return LIST_SERVER_REQUEST;
	if(strcmp(comando,"list_client")==0)
		return LIST_CLIENT_REQUEST;
	if(strcmp(comando,"get_sync_dir")==0)
		return GET_SYNC_DIR;
	if(strcmp(comando,"exit")==0)
		return EXIT_REQUEST;

	puts("entrada invalida");

	return -1;
}

int menu(int sockfd){

	char arq[BUFFER_SIZE],buffer[BUFFER_SIZE];
	int conectado = 1,comando,n;

	printf("\n\n\n\nComandos\n"
		   "%s\n"
		   "%s\n"
		   "%s\n"
		   "%s\n"
		   "%s\n"
		   "%s\n"
		   "-----------------------\n","upload <path/filename.ext>","download <filename.ext> ","list_server","list_client","get_sync_dir","exit");

	while(conectado){

		bzero(buffer,BUFFER_SIZE);

		do{
			comando = get_input(arq);
		}while(comando < 0);

		sem_wait(&operacao_completa);
		remove_notify();

		struct file_list fl;
		bzero(buffer,BUFFER_SIZE);
		switch (comando){
			case UPLOAD_REQUEST:
				sprintf(buffer,"%d",UPLOAD_REQUEST);
				n = write(sockfd, buffer, BUFFER_SIZE);
				if(n<0){
						perror(ERR_SOCKET_W);
						break;
				}
				n = send_file_to_server(arq,sockfd);
  				if(n != 0){
  					perror("erro ao enviar arquivo");
  					break;
  				}
  				puts("arquivo enviado com sucesso");
				break;
			case DOWNLOAD_REQUEST:
				sprintf(buffer,"%d",DOWNLOAD_REQUEST);
				n = write(sockfd, buffer, BUFFER_SIZE);
				if(n<0){
						perror(ERR_SOCKET_W);
						break;
					}
				n = get_file_from_server(arq,sockfd);
				if(n == 0){
  					perror("erro ao receber arquivo");
  					break;
				}
  				puts("arquivo receber com sucesso");
				break;
			case LIST_SERVER_REQUEST:
				sprintf(buffer,"%d",LIST_SERVER_REQUEST);
				n = write(sockfd, buffer, BUFFER_SIZE);
				if(n<0){
						perror(ERR_SOCKET_W);
						break;
					}
				puts("arquivos no servidor:");
				get_server_file_list(&fl,sockfd);
				print_file_list(fl);
				break;
			case LIST_CLIENT_REQUEST:
				puts("arquivos no cliente:");
				list_files(user_folder,&fl);
				print_file_list(fl);
				break;
			case GET_SYNC_DIR:
				n = get_sync_dir();
				if(n != 0){
			    	perror("diretório não foi criado, verifique se ele já existe!\n");
			    	break;
			    }
			    printf("diretório criado com sucesso - %s\n",user_folder);
				break;
			case EXIT_REQUEST:
				sprintf(buffer,"%d",EXIT_REQUEST);
				n = write(sockfd, buffer, BUFFER_SIZE);
				if (n < 0){
					perror(ERR_SOCKET_W);
					break;
    			}
    			bzero(buffer,BUFFER_SIZE);
				n = recv(sockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
				if (n < 0){
					perror(ERR_SOCKET_R);
					break;
    			}
    			if(strcmp(DISCONNECTED,buffer) != 0){
    				perror("erro ao se desconectar do servidor");
    				break;
    			}
    			puts("desconectado do servidor");
				break;
		}

		add_notify();
		sem_post(&operacao_completa);

		if(n < 0){
			perror("abortando execução");
			exit(1);
		}

	}

	return 0;
}

int get_server_file_list(struct file_list *fl,int sockfd){

	char buffer[sizeof(struct file_list)];
	int recebidos;

	bzero(buffer,BUFFER_SIZE);

	recebidos = recv(sockfd, buffer, sizeof(struct file_list),MSG_WAITALL);
	if (recebidos < 0){
		perror(ERR_SOCKET_R);
		exit(1);
	}

	struct file_list * servidor = (struct file_list *)buffer;
	fl->length = servidor->length;
	int i=0;
	for(;i<servidor->length;i++){
		strcpy(fl->f[i].name,servidor->f[i].name);
		fl->f[i].size = servidor->f[i].size;
		fl->f[i].last_modified = servidor->f[i].last_modified;
	}
	return 0;
}

int send_file_to_server(char *name,int sockfd){

	char buffer[BUFFER_SIZE];
	int n;

	bzero(buffer,BUFFER_SIZE);

	int offset = strlen(name);
	for(;name[offset] != '/' && offset >= 0;offset--);

	sprintf(buffer,"%s",name+offset+1);

	printf("enviando arquivo %s para o servidor...\n",buffer);

	//envia o nome do arquivo
	n = write(sockfd, buffer, BUFFER_SIZE);
    if (n < 0){
		perror(ERR_SOCKET_W);
		exit(1);
    }

    if( 0 != send_file(name,sockfd)){
    	perror("erro no envio do arquivo");
    	exit(1);
    }

	return 0;
}

int get_file_from_server(char *name,int sockfd){

	char buffer[BUFFER_SIZE];
	int n;

	int offset = strlen(name);
	for(;name[offset] != '/' && offset >= 0;offset--);

	bzero(buffer,BUFFER_SIZE);
	sprintf(buffer,"%s",name+offset+1);

	n = write(sockfd, buffer, BUFFER_SIZE);
    if (n < 0){
		perror(ERR_SOCKET_W);
		exit(1);
    }

    printf("recebendo arquivo %s do servidor...\n",buffer);

	//recebe confirmação se o arquivo existe
	bzero(buffer,BUFFER_SIZE);
	n = recv(sockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
    if (n < 0){
		perror(ERR_SOCKET_R);
		exit(1);
    }
    if(strcmp(buffer,NOT_FOUND) == 0){
    	perror("arquivo não existe no servidor");
    	return 0;
    }

    return receive_file(name,sockfd);
}

int remove_file_from_server(char *name,int sockfd){

	char buffer[BUFFER_SIZE];
	int n;

	int offset = strlen(name);
	for(;name[offset] != '/' && offset >= 0;offset--);

	bzero(buffer,BUFFER_SIZE);
	sprintf(buffer,"%s",name+offset+1);
	//envia o nome do arquivo
	n = write(sockfd, buffer, BUFFER_SIZE);
    if (n < 0){
		perror(ERR_SOCKET_W);
		exit(1);
    }

	//recebe confirmação se o arquivo existe
	bzero(buffer,BUFFER_SIZE);
	n = recv(sockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
    if (n < 0){
		perror(ERR_SOCKET_R);
		exit(1);
    }

    if(strcmp(buffer,NOT_DELETED) == 0){
    	return -1;
    }

 	return 0;
}

int get_sync_dir(){
	char *home = getenv ("HOME");
    sprintf(user_folder,"%s/sync_dir_%s",home,user_name);
    int n = create_folder(user_folder);
    return n;
}


int main(int argc,char *argv[]){

	int porta, sockfd, n;
	pthread_t soldado_server_id,soldado_cliente_id;

    struct sockaddr_in serv_addr;

    char buffer[BUFFER_SIZE];
    char id_usuario[MAX_NAME];

	if(argc < 4){
		perror("necessario informar porta, ip do servidor e nome do usuario\n");
		exit(1);
	}

	puts("iniciando cliente");

	porta = atoi(argv[1]);
	strcpy(id_usuario,argv[3]);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1){
        perror("erro abrir a socket\n");
    }

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(porta);

	 if(inet_pton(AF_INET, argv[2], &serv_addr.sin_addr)<=0)
    {
        perror("erro no ip fornecido!\n");
        exit(1);
    }

	bzero(&(serv_addr.sin_zero), 8);

	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){
        perror("erro na conexao com o servidor\n");
	}

    bzero(buffer, BUFFER_SIZE);
    strcpy(buffer,id_usuario);


		printf("WRITE CLIENTE.C 327\n");
	n = write(sockfd, buffer, BUFFER_SIZE);
    if (n < 0){
		perror(ERR_SOCKET_W);
		exit(1);
    }

    strcpy(user_name,argv[3]);
    n = get_sync_dir();
    if(n == 0){
    	printf("Seja bem vindo, sua pasta esta sendo criada em:%s\n",user_folder);
    }

    bzero(buffer,BUFFER_SIZE);
    n = recv(sockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
    if (n < 0){
		perror(ERR_SOCKET_R);
		exit(1);
    }

    //confirmação da conexão, segue para menu de opções
    if(strcmp(buffer,CONNECTED) == 0){
    	sem_init(&operacao_completa,0,1);
    	pthread_create(&soldado_server_id,NULL,keepServerSyncSoldier,(void *)&sockfd);
    	pthread_create(&soldado_cliente_id,NULL,keepClientSyncSoldier,(void *)&sockfd);
    	puts(CONNECTED);
    	menu(sockfd);
    	sem_destroy(&operacao_completa);
    }
    else{
    	perror("erro na conexao com o servidor\n");
    }

	close(sockfd);

	return 0;
}
