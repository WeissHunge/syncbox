#include "sync_client.h"


static int fd = 0;
static int wd = 0;


/*
                           _                                   __                         _       _           _
 _   _ ___  __ _ _ __   __| | ___    ___  ___ _ __ ___   __ _ / _| ___  _ __ ___     __ _| | ___ | |__   __ _| |
| | | / __|/ _` | '_ \ / _` |/ _ \  / __|/ _ \ '_ ` _ \ / _` | |_ / _ \| '__/ _ \   / _` | |/ _ \| '_ \ / _` | |
| |_| \__ \ (_| | | | | (_| | (_) | \__ \  __/ | | | | | (_| |  _| (_) | | | (_) | | (_| | | (_) | |_) | (_| | |
 \__,_|___/\__,_|_| |_|\__,_|\___/  |___/\___|_| |_| |_|\__,_|_|  \___/|_|  \___/   \__, |_|\___/|_.__/ \__,_|_|
                                                                                    |___/

*/

extern sem_t operacao_completa;
extern char user_folder[MAX_NAME];

int remove_notify(){


	return inotify_rm_watch(fd,wd);
}

int add_notify(){

  	wd = inotify_add_watch( fd, user_folder, IN_ALL_EVENTS);
  	return 0;
}

int validFile(char file_name[]){

	int end = strlen(file_name) - 1;

	if(file_name[end] == '~')
		return -1;

	if(file_name[0] == '.')
		return -2;

	char * pos = strrchr(file_name,'.');

	if(pos == NULL)
		return 1;
	
	if(strcmp(pos+1,"crdownload") == 0)
		return -3;

	char * temp = strstr(pos+1,"temp");
	if(temp != NULL)
		return -4;

	temp = strstr(pos+1,"swp");
	if(temp != NULL)
		return -5;


	return 1;
}

void* keepServerSyncSoldier(void *p1){

	int sockfd = *(int*)p1;

	int tamanho,n,i;
	char event_buffer[EVENT_BUF_LEN],buffer[BUFFER_SIZE];
	fd = inotify_init();

	if ( fd < 0 ) {
    	perror("erro inotify_init na thread secundaria, abortando....\n");
    	exit(1);
  	}

  	
  	add_notify();
  	while(1){

	  	tamanho = read( fd, event_buffer, EVENT_BUF_LEN);
	  	if ( tamanho < 0 ) {
	    	perror( "erro leitura inotify na thread secundaria, abortando....\n" );
	    	exit(1);
	  	}
	  	i = 0;
		sem_wait(&operacao_completa);
	  	while ( i < tamanho ) {
	  		struct inotify_event *event = ( struct inotify_event * ) &event_buffer[ i ];
	  		//evita arquivos ocultos e temporários
	  		if ( validFile(event->name) == 1) {
	      		if ( event->mask & (IN_CLOSE_WRITE | IN_CREATE | IN_MOVED_TO) ) {
	      			printf("%d\n",event->mask & (IN_CLOSE | IN_CREATE));
	      			usleep(1000);
			        if ( event->mask & IN_ISDIR ) {
			          printf( "nao crie diretorios, somente arquivos podem ser sincronizados (%s)\n", event->name );
			        }
			        else {
			        	sprintf(buffer,"%d",UPLOAD_REQUEST);
						n = write(sockfd, buffer, BUFFER_SIZE);
						if(n<0){
							perror(ERR_SOCKET_W);
							break;
						}
						sprintf(buffer,"%s/%s",user_folder,event->name);

	  					remove_notify();
						n = send_file_to_server(buffer,sockfd);
  						add_notify();
						if(n<0){
							perror("erro no envio do arquivo");
							break;
						}
			        }
	      		}
			      else if ( event->mask & (IN_DELETE | IN_MOVED_FROM) ) {
			        if ( event->mask & IN_ISDIR ) {
			          printf( "elimine todos os diretorios...(- %s)\n", event->name );
			        }
			        else {
			        	sprintf(buffer,"%d",REMOVE_REQUEST);
						n = write(sockfd, buffer, BUFFER_SIZE);
						if(n<0){
							perror(ERR_SOCKET_W);
							break;
						}
						sprintf(buffer,"%s/%s",user_folder,event->name);
						remove_notify();
						remove_file_from_server( buffer ,sockfd);
						add_notify();
						if(n<0){
							perror("erro no envio do arquivo");
							break;
						}
			        }
			      }
			    }
	    	i += EVENT_SIZE + event->len;
		}

		sem_post(&operacao_completa);
		if(n < 0){
			perror("abortando o programa");
			exit(1);
		}

	}


	return 0;
}

int syncDiffFilesList(struct file_list* client_fl,struct file_list* server_fl,int sockfd){

	int i=0,j=0,n,existe,arq_marcados[client_fl->length];
	char *nome_arq_cliente,*nome_arq_servidor,buffer[BUFFER_SIZE];

	for(i=0;i<client_fl->length;i++)
		arq_marcados[i] = 0;

	for(i=0;i<server_fl->length;i++){

		existe = 0;
		nome_arq_servidor = strrchr(server_fl->f[i].name,'/');
		if(nome_arq_servidor == NULL)
			nome_arq_servidor = server_fl->f[i].name;
		else
			nome_arq_servidor++;

		for(j=0;j<client_fl->length;j++){

			nome_arq_cliente = strrchr(client_fl->f[j].name,'/');
			if(nome_arq_cliente == NULL)
				nome_arq_cliente = client_fl->f[j].name;
			else
				nome_arq_cliente++;

			if((strcmp(nome_arq_cliente,nome_arq_servidor) == 0) && (client_fl->f[j].last_modified > server_fl->f[i].last_modified)){
				existe = 1;
				arq_marcados[j] = 1;
			}

		}
		if(existe == 0){
			sprintf(buffer,"%d",DOWNLOAD_REQUEST);
			n = write(sockfd, buffer, BUFFER_SIZE);
			if(n<0){
				perror(ERR_SOCKET_W);
				exit(1);
			}
			sprintf(buffer,"%s/%s",user_folder,nome_arq_servidor);
			n = get_file_from_server(buffer,sockfd);
			if(n != 0){
				perror("erro fatal na leitura arquivo do servidor,abortando");
				exit(3);
			}
		}

	}

	for(j=0;j<client_fl->length;j++){
		if(arq_marcados[j] != 1){
			n = remove(client_fl->f[j].name);
			if(n != 0){
				perror("não foi possível remover arquivo do servidor,abortando");
				exit(3);
			}
		}
	}

	return 0;
}

int doIhaveToSync(int sockfd){

	int n;
	char buffer[BUFFER_SIZE];

	sprintf(buffer,"%d",SYNC_REQUEST);
	n = write(sockfd, buffer, BUFFER_SIZE);
	if(n<0){
		perror(ERR_SOCKET_W);
		exit(1);
	}

	bzero(buffer,BUFFER_SIZE);
	n = recv(sockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
	if(n<0){
		perror(ERR_SOCKET_R);
		exit(1);
	}
	return atoi(buffer);
}

void* keepClientSyncSoldier(void *p1){

	char buffer[BUFFER_SIZE];
	int n,sockfd = *(int*)p1,sync_need;
	struct file_list server_fl,client_fl;

	while(1){

		while(1){
			sem_wait(&operacao_completa);
			sync_need = doIhaveToSync(sockfd);
			sem_post(&operacao_completa);
			if(1 == sync_need){
				break;
			}
			else{
				usleep(1000);
			}
		}

		sem_wait(&operacao_completa);
		remove_notify();

		sprintf(buffer,"%d",LIST_SERVER_REQUEST);
		n = write(sockfd, buffer, BUFFER_SIZE);
		if(n<0){
			perror(ERR_SOCKET_W);
			break;
		}

		n = get_server_file_list(&server_fl,sockfd);
		if(n!=0){
			perror("não foi possível ler a lista de arquivos do servidor");
			break;
		}
		n = list_files(user_folder,&client_fl);
		if(n!=0){
			perror("não foi possível ler a lista de arquivos do cliente");
			break;
		}
		n = syncDiffFilesList(&client_fl,&server_fl,sockfd);
		if(n!=0){
			perror("nao foi possivel comparar as listas dos usuários");
			break;
		}

		add_notify();
		sem_post(&operacao_completa);


	}
	if(n<0){
		perror("abortando thread secundária...");
		sem_post(&operacao_completa);
		exit(2);
	}

	return 0;
}
