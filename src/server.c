#include "server.h"


sem_t acesso_metadados;

int get_file_from_client(char *path,int sockfd,fila *atual){
	char buffer[BUFFER_SIZE];
	int n;

	//leitura do nome do arquivo
	n = recv(sockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
    if (n < 0){
		perror(ERR_SOCKET_R);
		exit(1);
    }

    char nome[MAX_NAME];
    sprintf(nome,"%s/%s",path,buffer);

    int indice_arq = writeSemWait(nome,atual);
    //quando esta recebendo arquivo novo não há necessidade de protteção,
    // irá retornar -1, quando houver mudança de arquivo já existente deve
    //haver proteção

    printf("upload do cliente, arquivo: %s\n",nome);

    if(0 == receive_file(nome,sockfd))
    	printf("arquivo recebido: %s\n",nome);
    else
    	perror("nao foi possivel receber o arquivo\n");

    writeSemPost(indice_arq,atual);

    return 0;
}

int send_file_to_client(char *path,int sockfd,fila *atual){
	char buffer[BUFFER_SIZE];
	int n,encontrado;

	//leitura do nome do arquivo
	n = recv(sockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
    if (n < 0){
		perror(ERR_SOCKET_R);
		exit(1);
    }
    char nome[MAX_NAME];
    sprintf(nome,"%s/%s",path,buffer);

    int indice_arq = readSemWait(nome,atual);

    printf("download do cliente, arquivo: %s\n",nome);

    FILE *fp = fopen(nome,"r");
    encontrado = 0;
    if(fp == NULL){
    	strcpy(buffer,NOT_FOUND);
    }
    else{
    	encontrado = 1;
    	fclose(fp);
    	strcpy(buffer,FOUND);
    }
    n = write(sockfd, buffer, BUFFER_SIZE);
    if (n < 0){
		perror(ERR_SOCKET_W);
		readSemPost(indice_arq,atual);
		exit(1);
    }
    if(encontrado == 1){
    	send_file(nome,sockfd);
    	readSemPost(indice_arq,atual);
    	return 0;
    }

    readSemPost(indice_arq,atual);
    return -1;

}

int delete_file_from_client(char *path,int sockfd,fila *atual){

	char buffer[BUFFER_SIZE];
	int n;

	//leitura do nome do arquivo
	n = recv(sockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
    if (n < 0){
		perror(ERR_SOCKET_R);
		exit(1);
    }

    char nome[MAX_NAME];
    sprintf(nome,"%s/%s",path,buffer);

    int indice_arq = writeSemWait(nome,atual);

    printf("remover arquivo do cliente, arquivo: %s\n",nome);

    n = remove(nome);

    bzero(buffer,BUFFER_SIZE);
    strcpy(buffer,DELETED);
    if(n < 0)
    	strcpy(buffer,NOT_DELETED);

    n = write(sockfd, buffer, BUFFER_SIZE);
    if (n < 0){
		perror(ERR_SOCKET_W);
		writeSemPost(indice_arq,atual);
		exit(1);
    }
    writeSemPost(indice_arq,atual);
    return 0;
}

int send_file_list(char *path,int sockfd,fila *atual){

	struct file_list *fl = &(atual->cliente.arquivos);
	list_files(path,fl);
	char *buffer;
	int n=0,enviados;

	buffer = (char *)fl;
	do{
		enviados = write(sockfd, buffer+n, sizeof(struct file_list)-n);
	    if (enviados < 0){
			perror(ERR_SOCKET_W);
			exit(1);
	    }
	    n += enviados;

	} while(n!=sizeof(struct file_list));

	return 0;
}

int dispatcher(int opcao,char *userfolder,int newsockfd,fila *atual,int device_atual){

	int n;
	char buffer[BUFFER_SIZE];

	switch(opcao){
		case UPLOAD_REQUEST:
			printf("pedido de upload - usuario %s dispositivo %d\n",atual->cliente.userid,device_atual);
			get_file_from_client(userfolder,newsockfd,atual);
			sem_wait(&atual->cliente.acess_client);
			atual->cliente.devices[(device_atual+1)%DEVICES] = 1;
			list_files(userfolder,&atual->cliente.arquivos);
			sem_post(&atual->cliente.acess_client);
			break;
		case REMOVE_REQUEST:
			printf("pedido de remocao - usuario %s dispositivo %d\n",atual->cliente.userid,device_atual);
			delete_file_from_client(userfolder,newsockfd,atual);
			sem_wait(&atual->cliente.acess_client);
			atual->cliente.devices[(device_atual+1)%DEVICES] = 1;
			list_files(userfolder,&atual->cliente.arquivos);
			sem_post(&atual->cliente.acess_client);
			break;
 		case DOWNLOAD_REQUEST:
 			printf("pedido de download - usuario %s dispositivo %d\n",atual->cliente.userid,device_atual);
 			n = send_file_to_client(userfolder,newsockfd,atual);
 			if(n < 0){
 				printf("arquivo nao enviado - %s\n",userfolder);
 			}
 			else{
 				printf("arquivo enviado -%s\n",userfolder);
 			}
 			break;
 		case LIST_SERVER_REQUEST:
 			//printf("pedido de list - usuario %s dispositivo %d\n",atual->cliente.userid,device_atual);
 			send_file_list(userfolder,newsockfd,atual);
 			break;
		case SYNC_REQUEST:
			sem_wait(&atual->cliente.acess_client);
			int need_sync = atual->cliente.devices[device_atual];
			//if(need_sync == 1)
			//	printf("sync do usuario %s dispositivo %d\n",atual->cliente.userid,device_atual);
			sprintf(buffer,"%d",need_sync);
			n = write(newsockfd, buffer, BUFFER_SIZE);
		    if (n < 0){
				perror(ERR_SOCKET_W);
				sem_post(&atual->cliente.acess_client);
				exit(1);
		    }
		    atual->cliente.devices[device_atual] = 0;
			sem_post(&atual->cliente.acess_client);
			break;
	}
	return 0;
}

void* soldado(void * sockfd){

	char buffer[BUFFER_SIZE],*home,client_folder[MAX_NAME];
	int newsockfd = *(int*)sockfd,n;

	home = getenv ("HOME");

	bzero(buffer,BUFFER_SIZE);
	n = recv(newsockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
	if (n < 0){
		perror(ERR_SOCKET_R);
		return (void*)NULL;
	}

	//primeiro obtemos o nome do usuario
	sprintf(client_folder,"%s/syncbox/",home);
	create_folder(client_folder);
	strcat(client_folder,buffer);
	if(create_folder(client_folder) == 0 ){
		printf("novo cliente:%s nova pasta:%s\n",buffer,client_folder);
	}

	sem_wait(&acesso_metadados);

	//verificacao se o cliente ja esta conectado
	n = existClient(buffer);
	if(n == 0){
		printf("conexao cliente %s\n",buffer);
		addCliente(buffer,client_folder);
	}

	sem_post(&acesso_metadados);

	//busca do cliente na fila de clientes afim de utilizar
	//os semáforos para controle de operações
	sem_wait(&acesso_metadados);

	fila* cliente_atual = getClient(buffer);

	sem_post(&acesso_metadados);


	sem_wait(&cliente_atual->cliente.acess_client);
	
	cliente_atual->cliente.logged_in++;

	//numero máximo de dispositivos,
	//recusa conexao para evitar erros
	if(cliente_atual->cliente.logged_in > DEVICES){
		cliente_atual->cliente.logged_in--;
		bzero(buffer,BUFFER_SIZE);
		n = write(newsockfd,buffer, BUFFER_SIZE);
		if (n < 0){
			perror(ERR_SOCKET_W);
		}
		sem_post(&cliente_atual->cliente.acess_client);
		exit(1);
	}

	//identificador do cliente atual
	int device_atual = (cliente_atual->cliente.lst_dev);
	(cliente_atual->cliente.lst_dev) = (cliente_atual->cliente.lst_dev + 1)%DEVICES;
	printf("dispositivo %d conectado\n",device_atual);

	sem_post(&cliente_atual->cliente.acess_client);


	bzero(buffer,BUFFER_SIZE);
	strcpy(buffer,CONNECTED);

	//confirmamos a conexão
	n = write(newsockfd,buffer, BUFFER_SIZE);
	if (n < 0){
		perror(ERR_SOCKET_W);
		exit(1);
	}

	int desejo;
	while(1){


		bzero(buffer,BUFFER_SIZE);
		n = recv(newsockfd, buffer, BUFFER_SIZE,MSG_WAITALL);
		if (n < 0){
			perror(ERR_SOCKET_R);
			exit(1);
		}
		desejo = atoi(buffer);
		if(desejo != EXIT_REQUEST){
			dispatcher(desejo,client_folder,newsockfd,cliente_atual,device_atual);
		}
		else{
			printf("fim da conexão - %s dispositivo %d\n",cliente_atual->cliente.userid,device_atual);
			bzero(buffer,BUFFER_SIZE);
			strcpy(buffer,DISCONNECTED);
			n = write(newsockfd,buffer, BUFFER_SIZE);
			if (n < 0){
				perror(ERR_SOCKET_W);
				exit(1);
			}
			break;
		}
	}

	sem_wait(&cliente_atual->cliente.acess_client);
	cliente_atual->cliente.logged_in--;
	cliente_atual->cliente.lst_dev = device_atual;
	cliente_atual->cliente.devices[device_atual] = 1;
	sem_post(&cliente_atual->cliente.acess_client);

	close(newsockfd);

	return 0;
}

int main(int argc,char *argv[]){

	int sockfd, newsockfd, porta;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;
	pthread_t soldado_id;
	sem_init(&acesso_metadados,0,1);

	if(argc < 2){
		perror("necessario informar a porta\n");
		exit(1);
	}

	porta = atoi(argv[1]);

	puts("iniciando servidor");

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
        perror("erro abrindo a socket\n");
    }

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(porta);
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(serv_addr.sin_zero), 8);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		perror("erro no bind do socket\n");
	}

	listen(sockfd, MAX_CONNECTIONS);

	while(1){
		clilen = sizeof(struct sockaddr_in);
		if ((newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen)) == -1){
			perror("erro no aceitar conexao\n");
		}
		pthread_create(&soldado_id,NULL,soldado,(void *)&newsockfd);

	}


	close(sockfd);

	return 0;
}
