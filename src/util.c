	#include "util.h"

	int create_folder(char *name){
		struct stat st = {0};

		if (stat(name, &st) == -1) {
		    if(0 != mkdir(name, 0777)){
		    	fprintf(stderr, "erro na criacao de %s\n",name);
		    	return -1;
		    }
		    else{
		    	return 0;
		    }
		}

		return -1;
	}

	void print_file_list(struct file_list fl){

		int i=0,offset;
		for(;i<fl.length;i++){
			offset = strlen(fl.f[i].name);
			for(;fl.f[i].name[offset] != '/' && offset >= 0;offset--);
			char *ultima_mudanca = ctime(&(fl.f[i].last_modified));
			printf("nome:%s tamanho:%ld ultima mudanca:%s",fl.f[i].name+offset+1,fl.f[i].size,ultima_mudanca);
		}
	}

	int list_files(char *dir_alvo,struct file_list* arquivos){
	  DIR *d;
	  struct stat sb;
	  struct dirent *dir;
	  d = opendir(dir_alvo);
	  char complete_path[MAX_NAME*2];

	  arquivos->length = 0;
	  if (d){
	    while ((dir = readdir(d)) != NULL){

	    	sprintf(complete_path,"%s/%s",dir_alvo,dir->d_name);

		      if (dir->d_name[0] == '.' || stat(complete_path, &sb) == -1) {
		      	continue;
		  	  }
		  	  //char *ultima_mudanca = ctime(&sb.st_mtime);
		  	  strcpy(arquivos->f[arquivos->length].name,complete_path);
			  arquivos->f[arquivos->length].last_modified = sb.st_mtime;
			  arquivos->f[arquivos->length].size = sb.st_size;

		  	  arquivos->length++;
	    }

	    closedir(d);
	  }
	  return 0;
	}

	int get_file_size(FILE *fp){

		rewind(fp);
		fseek(fp, 0L, SEEK_END);
		int size = ftell(fp);
		rewind(fp);



		return size;
	}

	int send_file(char *name,int socketid){
		char buffer[BUFFER_SIZE];
		int n,file_size, enviados;

		FILE * fp = fopen(name,"rb");
		if(fp == NULL)
			return -1;

		//printf("---------\nARQUIVO: %s\n",name);
		file_size = get_file_size(fp);


	  	int total_lidos=0,total_enviados=0;


		bzero(buffer,BUFFER_SIZE);
		sprintf((char*)buffer,"%d",file_size);
		printf("SIZE: %d\n", file_size);
		n = write(socketid, buffer, BUFFER_SIZE);
		//printf("WRITE 83\n");
	    if (n < 0){
			perror(ERR_SOCKET_W);
			exit(1);
	    }
	    enviados = 0;
	    while(file_size > enviados){
	    	n = fread(buffer,1,BUFFER_SIZE,fp);
	    	total_lidos+=n;
	    	//printf("lidos %d total %d \n",n,total_lidos);

	    	if (n < 0){
					perror("erro leitura de arquivo");
					exit(1);
		    }

	    	n = write(socketid, buffer, BUFFER_SIZE);
		    enviados += n;
		    total_enviados+=n;
				//printf("enviados %d total %d\n",n,total_enviados);
		    if (n < 0){
				perror(ERR_SOCKET_W);
				exit(1);
		    }
	    }
		puts("fim enviado");
	    fclose(fp);

		return 0;
	}

	int check_buffer(char *buffer){

		int zeros=0;
		int i;
		for (i=0; i< BUFFER_SIZE;i++) {
			if(buffer[i] == '\0')
				zeros++;
		}
		return (zeros > 1000);
	}


	void DumpHex(const void* data, size_t size) {
		char ascii[17];
		size_t i, j;
		ascii[16] = '\0';
		for (i = 0; i < size; ++i) {
			printf("%02X ", ((unsigned char*)data)[i]);
			if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <=
	'~') {
				ascii[i % 16] = ((unsigned char*)data)[i];
			} else {
				ascii[i % 16] = '.';
			}
			if ((i+1) % 8 == 0 || i+1 == size) {
				printf(" ");
				if ((i+1) % 16 == 0) {
					printf("|  %s \n", ascii);
				} else if (i+1 == size) {
					ascii[(i+1) % 16] = '\0';
					if ((i+1) % 16 <= 8) {
						printf(" ");
					}
					for (j = (i+1) % 16; j < 16; ++j) {
						printf("   ");
					}
					printf("|  %s \n", ascii);
				}
			}
		}
	}

	int receive_file(char *name,int socketid){
		
		char buffer[BUFFER_SIZE];
		int file_size,lidos;

		bzero(buffer,BUFFER_SIZE);
		int n = recv(socketid, buffer, BUFFER_SIZE,MSG_WAITALL);
		if (n < 0){
			perror(ERR_SOCKET_R);
			exit(1);
	  	}

		//printf("BUFFER: %s\n", buffer);
	  	file_size = atoi(buffer);
		//printf("SIZE: %d\n", file_size);
	  	FILE * fp = fopen(name,"wb");
		  if(fp == NULL){
		  	return -1;
		  }

	  	lidos = 0;
	  	while(file_size > lidos){
	    			bzero(buffer,BUFFER_SIZE);
					int result = recv(socketid, buffer, BUFFER_SIZE,MSG_WAITALL);
					lidos += result;
					//printf("leu %d total:%d\n",result,lidos);
					if (file_size - lidos < 0) {
						fwrite(buffer, 1, file_size%BUFFER_SIZE, fp);
					} else {
						fwrite(buffer, 1, BUFFER_SIZE, fp);
					}

					if (result < 0){
						//printf("Xiii...");
						perror(ERR_SOCKET_W);
						exit(1);
			    	}
		}

		puts("fim recebido");
	    fclose(fp);

		return 0;
	}

	static fila *fila_cliente = NULL;

	void addCliente(char * id,char *dir){

		if(fila_cliente == NULL){
			fila_cliente = malloc(sizeof(fila));
			fila_cliente->proximo = NULL;

			strcpy(fila_cliente->cliente.userid,id);
			int i=0;
			for(;i<MAX_FILES;i++){
				sem_init(&(fila_cliente->cliente.acess_file[i]),0,1);
				fila_cliente->cliente.leitores[i] = 0;
			}

			for(i=0;i<DEVICES;i++)
				fila_cliente->cliente.devices[i] = 1;

			list_files(dir,&(fila_cliente->cliente.arquivos));
			fila_cliente->cliente.lst_dev = 0;
			fila_cliente->cliente.logged_in = 0;
			sem_init(&(fila_cliente->cliente.acess_client),0,1);

			return;
		}

		printf("aqui");
		fila *pos_atual = fila_cliente;
		while(pos_atual->proximo != NULL){
			pos_atual = pos_atual->proximo;
		}
		pos_atual->proximo = malloc(sizeof(fila));
		strcpy(pos_atual->proximo->cliente.userid,id);
		int i=0;
		for(;i<MAX_FILES;i++){
			sem_init(&(pos_atual->proximo->cliente.acess_file[i]),0,1);
			pos_atual->proximo->cliente.leitores[i] = 0;
		}

		for(i=0;i<DEVICES;i++)
			pos_atual->proximo->cliente.devices[i] = 1;

		list_files(dir,&(pos_atual->proximo->cliente.arquivos));
		pos_atual->proximo->proximo = NULL;
		pos_atual->proximo->cliente.lst_dev = 0;
		pos_atual->proximo->cliente.logged_in = 0;
		sem_init(&(pos_atual->proximo->cliente.acess_client),0,1);
		printf("erro");
	}

	void removeCliente(char * id){

		if(fila_cliente == NULL)
			return;

		fila *marcada;

		if(strcmp(fila_cliente->cliente.userid,id) == 0){
			marcada = fila_cliente;
			fila_cliente = fila_cliente->proximo;
			free(marcada);
			return;
		}

		fila *pos_atual = fila_cliente;
		while(pos_atual->proximo != NULL && strcmp(pos_atual->proximo->cliente.userid,id) != 0){
			pos_atual = pos_atual->proximo;
		}
		if(pos_atual->proximo == NULL)
			return;

		marcada = pos_atual->proximo;
		pos_atual->proximo = pos_atual->proximo->proximo;
		free(marcada);
	}

	int existClientInQ(fila * atual,char * id){

		if(atual == NULL)
			return 0;

		if(strcmp(atual->cliente.userid,id) == 0)
			return 1;

		return existClientInQ(atual->proximo,id);
	}

	int existClient(char * id){

		return existClientInQ(fila_cliente,id);
	}

	fila * getClientInQ(fila * atual,char * id){

		if(atual == NULL)
			return NULL;

		if(strcmp(atual->cliente.userid,id) == 0)
			return atual;

		return getClientInQ(atual->proximo,id);
	}

	fila * getClient(char * id){

		return getClientInQ(fila_cliente,id);
	}

	int findFileIndex(char * path,fila * fc){

		int n=0;

		sem_wait(&(fc->cliente.acess_client));

		char *nome_alvo = strrchr(path,'/'),*nome_arq;
		if(nome_alvo == NULL)
			nome_alvo = path;
		else
			nome_alvo++;

		int i;
		for(i=0;i<fc->cliente.arquivos.length;i++){
			nome_arq = strrchr(fc->cliente.arquivos.f[i].name,'/');
			if(nome_arq == NULL)
				nome_arq = fc->cliente.arquivos.f[i].name;
			else
				nome_arq++;
			if(strcmp(nome_arq,nome_alvo) == 0){
				n = 1;
				break;
			}
		}

		sem_post(&(fc->cliente.acess_client));
		if(n == 1)
			return i;
		else
			return -1;
	}

	int writeSemWait(char * path,fila * fc){

		int i = findFileIndex(path,fc);

		if(i < 0 )
			return -1;

		sem_wait(&fc->cliente.acess_file[i]);

		return i;
	}

	int readSemWait(char * path,fila * fc){

		int i = findFileIndex(path,fc);
		if(i < 0 )
			return -1;

		sem_wait(&fc->cliente.acess_client);

		if(fc->cliente.leitores[i] == 0){
			sem_wait(&(fc->cliente.acess_file[i]));
		}
		fc->cliente.leitores[i]++;

		sem_post(&fc->cliente.acess_client);

		return i;
	}

	int writeSemPost(int i,fila * fc){

		if(i < 0 )
			return -1;

		sem_post(&fc->cliente.acess_file[i]);

		return 0;
	}

	int readSemPost(int i,fila * fc){

		if(i < 0 )
			return -1;

		sem_wait(&fc->cliente.acess_client);

		fc->cliente.leitores[i]--;
		if(fc->cliente.leitores[i] == 0){
			sem_post(&(fc->cliente.acess_file[i]));
		}

		sem_post(&(fc->cliente.acess_client));

		return 0;
	}
